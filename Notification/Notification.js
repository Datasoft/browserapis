(async() => {
    // create and show the notification
    const showNotification = () => {
        // create a new notification
        const notification = new Notification('Twitter', {
            body: 'Yugarajan Wants you to connect in twitter',

        });

        // close the notification after 10 seconds
        setTimeout(() => {
            notification.close();
        }, 10 * 1000);
        
        // navigate to a URL when clicked
        notification.addEventListener('click', () => {

            window.open('https://twitter.com/SYugarajan', '_blank');
        });
    }

    // show an error message
    const showError = () => {
        const error = document.querySelector('.error');
        error.style.display = 'block';
        error.textContent = 'you cannot access this Site';
    }

    // check notification permission
    let granted = false;

    if (Notification.permission === 'granted') {
        granted = true;
    } else if (Notification.permission !== 'denied') {
        let permission = await Notification.requestPermission();
        granted = permission === 'granted' ? true : false;
    }

    // show notification or error
    granted ? showNotification() : showError();

})();