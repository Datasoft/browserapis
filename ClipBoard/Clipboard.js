function clipRun()
{
    navigator.clipboard.readText()
    .then(
        clipText => document.querySelector(".editor").innerText = clipText);
}
