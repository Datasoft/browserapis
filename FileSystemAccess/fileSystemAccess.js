function file(){
    let saveFile;
    document.getElementById('someButton').addEventListener('click', async () => {
        try {
            saveFile = await window.showSaveFilePicker({
                suggestedName: 'My third File.txt'
            });
            const file = await saveFile.getFile();
            const contents = await file.text();
        } catch(e) {
            console.log(e);
        }
    });
    }
    
    async function saveFile() {
    
        // create a new handle
        const newHandle = await window.showSaveFilePicker();
      
        // create a FileSystemWritableFileStream to write to
        const writableStream = await newHandle.createWritable(newHandle);
      
        // write our file
        await writableStream.write("WebAPI");
      
        // close the file and write the contents to disk.
        await writableStream.close();
      }