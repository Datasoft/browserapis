// emit non-blocking beacon to record client-side event
function reportEvent(event) {
    var data = JSON.stringify({
        event: event,
        time: performance.now()
    });
    navigator.sendBeacon('/collector', data);
}

// emit non-blocking beacon with session analytics as the page
// transitions to background state (Page Visibility API)
document.addEventListener('visibilitychange', function() {
    if (document.visibilityState === 'hidden') {
        var sessionData = buildSessionReport();
        navigator.sendBeacon('/collector', sessionData);
    }
});